package com.example.creativez.elektrimotask;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE =0 ;
    CircleImageView circleImageView;
    private static final int PICK_IMAGE_REQUEST = 1;

    private File actualImage;
    private ProgressDialog prg;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data == null) {
                showError("Failed to open picture!");
                return;
            }
            try {
                actualImage = FileUtil.from(this, data.getData());
//                customCompressImage();
                circleImageView.setImageBitmap(BitmapFactory.decodeFile(actualImage.getAbsolutePath()));
//                actualSizeTextView.setText(String.format("Size : %s", getReadableFileSize(actualImage.length())));
//                clearImage();
//                customCompressImage();
            } catch (IOException e) {
                showError("Failed to read picture data!");
                e.printStackTrace();
            }
        }
    }
    public void showError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        circleImageView=findViewById(R.id.addimage);

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                                View view=getLayoutInflater().inflate(R.layout.dialogbox,null);
                                Button choose=view.findViewById(R.id.choosefromgallery);
                                Button cancel=view.findViewById(R.id.canecl);

                                        builder.setView(view);
                               final AlertDialog dialog=builder.create();
                               dialog.show();


                               choose.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {

                                       ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);

                                       Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                       intent.setType("image/*");
                                       startActivityForResult(intent, PICK_IMAGE_REQUEST);
dialog.dismiss();





                                   }
                               });
                               cancel.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       dialog.dismiss();
                                   }
                               });



            }
        });




    }



}
